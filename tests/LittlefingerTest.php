<?php

namespace Tests\Goldeneye;

use Littlefinger\Littlefinger;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use PHPUnit\Framework\TestCase;

class LittlefingerTest extends TestCase
{
    public function testClientWithGnuSocialUser()
    {
        $httpClient = new GuzzleAdapter(new GuzzleClient());
        $client = new Littlefinger($httpClient, new GuzzleMessageFactory());
        $result = $client->finger("acct:rgggn@quitter.no");

        $this->assertEquals('application/jrd+json', $result->getMimeType());
        $this->assertEquals('acct:rgggn@quitter.no', $result->getSubject());
        $this->assertContains('https://quitter.no/rgggn', $result->getAliases());
        $this->assertCount(12, $result->getLinks());
    }

    public function testClientWithMastodonUser()
    {
        $httpClient = new GuzzleAdapter(new GuzzleClient());
        $client = new Littlefinger($httpClient,  new GuzzleMessageFactory());
        $result = $client->finger("acct:tcit@social.tcit.fr");

        $this->assertEquals('application/jrd+json', $result->getMimeType());
        $this->assertEquals('acct:tcit@social.tcit.fr', $result->getSubject());
        $this->assertContains('https://social.tcit.fr/@tcit', $result->getAliases());
        $this->assertCount(6, $result->getLinks());
    }
}