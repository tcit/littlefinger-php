<?php

namespace Littlefinger;

class Link
{

    /**
     * @var string
     */
    private $href;

    /**
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $titles;

    /**
     * @var string
     */
    private $rel;

    /**
     * @var array
     */
    private $properties;

    public function __construct(array $a)
    {
        $this->href = array_key_exists('href', $a) ? $a['href'] : null;
        $this->template = array_key_exists('template', $a) ? $a['template'] : null;
        $this->type = array_key_exists('type', $a) ? $a['type'] : null;
        $this->rel = array_key_exists('rel', $a) ? $a['rel'] : null;
        $this->titles = array_key_exists('titles', $a) ? $a['titles'] : null;
        $this->properties = array_key_exists('properties', $a) ? $a['properties'] : null;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     * @return Link
     */
    public function setHref($href)
    {
        $this->href = $href;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     * @return Link
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @param mixed $rel
     * @return Link
     */
    public function setRel($rel)
    {
        $this->rel = $rel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     * @return Link
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * @param mixed $titles
     * @return Link
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Link
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $lang
     * @return null|string
     */
    public function getTitle(string $lang): ?string
    {
        return in_array($lang, $this->titles) ? $this->titles[$lang] : null;
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getProperty(string $key): ?string
    {
        return in_array($key, $this->properties) ? $this->properties[$key] : null;
    }
}