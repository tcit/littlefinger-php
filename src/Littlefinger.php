<?php

namespace Littlefinger;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;

class Littlefinger {

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    public function __construct(HttpClient $httpClient, MessageFactory $messageFactory)
    {
        $this->httpClient = $httpClient;
        $this->messageFactory = $messageFactory;
    }

    /**
     * @param string $uri
     * @return Result
     */
    public function finger(string $uri): Result
    {
        $client = new Client($uri, $this->httpClient, $this->messageFactory);
        return $client->finger();
    }
}