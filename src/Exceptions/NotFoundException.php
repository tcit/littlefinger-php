<?php

namespace Littlefinger\Exceptions;

class NotFoundException extends WebFingerException {}