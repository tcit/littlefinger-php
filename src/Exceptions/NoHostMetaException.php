<?php

namespace Littlefinger\Exceptions;

class NoHostMetaException extends WebFingerException {}