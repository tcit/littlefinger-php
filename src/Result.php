<?php

namespace Littlefinger;

use DOMDocument;
use DOMElement;
use DOMXPath;
use Littlefinger\Exceptions\WebFingerException;
use Psr\Http\Message\ResponseInterface;

class Result {
    const MIME_TYPES_JSON = [
        'application/jrd+json',
        'application/json'
    ];
    const MIME_TYPES_XML = [
        'application/xrd+xml',
        'application/xml',
        'text/xml',
    ];

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var string
     */
    private $body;

    private $subject = null;

    /**
     * @var array
     */
    private $aliases;

    /**
     * @var array
     */
    private $links;

    /**
     * @var array
     */
    private $properties;

    /**
     * Result constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        if (count($response->getHeader('Content-Type')) > 0) {
             $contentType = $response->getHeader('Content-Type')[0];

            $this->mimeType = explode(';', $contentType)[0];
        }
        $this->body = $response->getBody();

        $this->parse();
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     * @return Result
     */
    public function setMimeType(string $mimeType): Result
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return Result
     */
    public function setBody(string $body): Result
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param null $subject
     * @return Result
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return array
     */
    public function getAliases(): array
    {
        return $this->aliases;
    }

    /**
     * @param array $aliases
     * @return Result
     */
    public function setAliases(array $aliases): Result
    {
        $this->aliases = $aliases;
        return $this;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * @param array $links
     * @return Result
     */
    public function setLinks(array $links): Result
    {
        $this->links = $links;
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return Result
     */
    public function setProperties(array $properties): Result
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getProperty(string $key): ?string
    {
        return in_array($key, $this->properties) ? $this->properties[$key] : null;
    }

    /**
     * @param string $rel
     * @return null|Link
     */
    public function getLink(string $rel): ?Link
    {
        return in_array($rel, array_keys($this->links)) ? $this->links[$rel] : null;
    }

    private function parse(): void
    {
        if (in_array($this->mimeType, self::MIME_TYPES_JSON)) {
            $this->parseJson();
        } elseif (in_array($this->mimeType, self::MIME_TYPES_XML)) {
            $this->parseXml();
        } else {
            throw new WebFingerException("Invalid response MIME type :" . $this->mimeType);
        }
    }

    private function parseJson(): void
    {
        $json = json_decode($this->body, true);

        $this->subject = $json['subject'];
        $this->aliases = array_key_exists('aliases', $json) ? $json['aliases'] : [];
        $this->properties = array_key_exists('properties', $json) ? $json['properties'] : [];

        foreach ($json['links'] as $link) {
            $this->links[$link['rel']] = new Link($link);
        }
    }

    private function parseXml(): void
    {
        $document = new DOMDocument();
        $document->loadXML($this->body);
        $xml = new DOMXPath($document);
        $this->subject = $xml->query('//xmlns:Subject')->item(0)->nodeValue;
        foreach ($xml->query('//xmlns:Alias') as $item) {
            $this->aliases[] = $item->nodeValue;
        }

        $localProperties = $xml->query('/xmlns:XRD/xmlns:Property');
        foreach ($localProperties as $prop) {
            /** @var $prop DOMElement */
            $this->properties[$prop->getAttribute('type')] = $prop->getAttribute('null') ? null : $prop->nodeValue;
        }

        foreach ($xml->query('//xmlns:Link') as $link) {
            /** @var DOMElement $link*/
            $rel = $link->getAttribute('rel');
            $tmp = [];
            foreach ($link->attributes as $attribute => $value) {
                $tmp[$attribute] = $value;
            }
            $tmp['titles'] = [];
            $tmp['properties'] = [];

            foreach ($xml->query('.//xmlns:Title', $link) as $title) {
                /** @var $title DOMElement */
                $tmp['titles'][$title->getAttribute('lang')] = $title->nodeValue;
            }
            foreach ($xml->query('.//xmlns:Property', $link) as $title) {
                /** @var $title DOMElement */
                $tmp['properties'][$title->getAttribute('type')] = $title->hasAttribute('null') ? null : $title->nodeValue;
            }

            $this->links[$rel] = new Link($tmp);
        }
    }

}