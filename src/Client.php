<?php

namespace Littlefinger;

use Littlefinger\Exceptions\NotFoundException;
use Littlefinger\Exceptions\WebFingerException;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Message\MessageFactory;
use Psr\Http\Message\ResponseInterface;

class Client {

    /**
     * @var string
     */
    private $uri;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    public function __construct(string $uri, HttpClient $client, MessageFactory $messageFactory)
    {
        $this->uri = $uri;
        $this->httpClient = $client ?: HttpClientDiscovery::find();
        $this->messageFactory = $messageFactory ?: MessageFactoryDiscovery::find();
    }

    public function finger(): Result
    {
        $url = $this->standardUrl();
        $response = $this->performGet($url);

        if ($response->getStatusCode() !== 200) {
            return $this->fingerFromTemplate();
        }
        return new Result($response);
    }

    private function fingerFromTemplate()
    {
        $template = $this->performGet($this->url());

        if ($template->getStatusCode() !== 200) {
            throw new NotFoundException('No host-meta on the server');
        }

        $response = $this->performGet($this->urlFromTemplate($template->getBody()));
        if ($response->getStatusCode() !== 200) {
            throw new NotFoundException('No such user on the server');
        }
        return new Result($response);
    }

    private function url(): string
    {
        return "https://" . $this->domain() . "/.well-known/host-meta";
    }

    private function standardUrl(): string
    {
        return "https://" . $this->domain() . "/.well-known/webfinger?resource=" . $this->uri;
    }

    private function urlFromTemplate(string $template): string
    {
        try {
            $document = new \DOMDocument();
            $document->loadXML($template);
            $xml = new \DOMXPath($document);

            $links = $xml->query('//xmlns:Link[@rel="lrdd"]');

            if (empty($links)) {
                throw new NotFoundException();
            }
            return preg_replace('{uri}', $this->uri, $links->item(0)->getAttribute('template'));
        } catch (\DOMException $e) {
            throw new WebFingerException("Bad XML :" . $template);
        }
    }

    private function domain(): string
    {
        $parts = explode('@', $this->uri);
        return end($parts);
    }

    /**
     * @param string $uri
     * @return ResponseInterface
     */
    private function performGet(string $uri): ResponseInterface
    {
        return $this->performRequest('GET', $uri);
    }

    /**
     * @param $method
     * @param $uri
     * @return ResponseInterface
     */
    private function performRequest(string $method, string $uri): ResponseInterface
    {
        $request = $this->messageFactory->createRequest($method, $uri);
        return $this->httpClient->sendRequest($request);
    }
}